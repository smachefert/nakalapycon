ressource utilisées :
https://towardsdatascience.com/how-to-publish-a-python-package-to-pypi-7be9dd5d6dcd > attention mauvais commande twine !

https://python-packaging.readthedocs.io/en/latest/index.html. 

----------


cd C:\.....\nakalapycon

activate base

python setup.py sdist bdist_wheel

pip install -e .

essai :
python
import nakalapycon

tt = nakalapycon.NklTarget(isNakalaProd=True, apiKey="")

ctrl+D

pip uninstall nakalapycon


## upload sur testpypi:
twine upload -r testpypi dist/*
username: ...
password: 
> view at https://test.pypi.org/project/nakalapycon/0.0.1/

essai :
pip install -i https://test.pypi.org/simple/ nakalapycon==0.0.2


## upload sur pypi
twine upload dist/* 
essai :
pip install nakalapycon




