# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 10:04:12 2021

@author: mnauge
"""


import NklTarget as nklT

def test_NklProd():
    """
    Tester la création d'un objet NklTarget production
    
    """
    
    myApiKey = "000000000-00ab-cdef-0000-000000abcdef"
    nklT_prod = nklT.NklTarget(isNakalaProd=True, apiKey=myApiKey)
    
    # on verifie que l'affectation des urls est bonne
    assert nklT_prod.BASE_URL == "https://nakala.fr"
    assert nklT_prod.API_URL == "https://api.nakala.fr"
    # on vérifie que notre ApiKey est bien affacté à la bonne variable dans notre objet
    assert nklT_prod.API_KEY_NKL == myApiKey   
    
    
def test_apiKey_isEmpty():
    
    # on vérifie que si on précise une ApiKey vide à un objet NklTarget
    # la valeur de retour est bien True
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")
    assert nklT_prodEmpty.apiKey_isEmpty() == True
    
    # on vérifie que si on précise une ApiKey à un objet NklTarget
    # la valeur de retour est bien False
    myApiKey = "000000000-00ab-cdef-0000-000000abcdef"
    nklT_prodFilled = nklT.NklTarget(isNakalaProd=True, apiKey=myApiKey)
    assert nklT_prodFilled.apiKey_isEmpty() == False
    
test_NklProd()
test_apiKey_isEmpty()