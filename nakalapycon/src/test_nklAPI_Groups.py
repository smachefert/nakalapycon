# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

@author: Michael Nauge (Université de Poitiers)
"""


import NklTarget as nklT
import nklAPI_Groups as nklG


def get_groups_test():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    
    
    print("Test d'un group Identifier qui n'existe pas >> ")  
    groupIdentifier = "db5f5980-056e-11ec-9b31-52540084ccd0"
    r = nklG.get_groups(nklT_test, groupIdentifier)
    print(r)   
    
    print("Test d'un group Identifier qui existe >>")
    groupIdentifier = "cb5f5980-056e-11ec-9b31-52540084ccd3"
    r = nklG.get_groups(nklT_test, groupIdentifier)
    if r.isSuccess:
        
        print("group finded")
        print("id : ", r.dictVals['id'])
        print("name:",r.dictVals['name'])
        print("type :", r.dictVals['type'])
        
        print("users")
        for user in r.dictVals['users']:
            print(user)
    else:
        print(r)
    
        
    
def put_groups_test():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    groupIdentifier = "cb5f5980-056e-11ec-9b31-52540084ccd3"
    
    
    dicoGroups = {}
    dicoGroups["id"] = "cb5f5980-056e-11ec-9b31-52540084ccd3"


    dicoGroups["name"] = "etablissement42"


    dicoGroups["users"] = []
    
    dicoGroups["users"].append("unakala1")
    dicoGroups["users"].append("unakala2")
    
    
    r = nklG.put_groups(nklT_test, groupIdentifier, dicoGroups)
    print(r)

    
def post_groups_test():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    dicoGroups = {}

    dicoGroups["name"] = "groupCreatedByAPI"

    dicoGroups["users"] = []
    
    dicoGroups["users"].append("unakala1")
    dicoGroups["users"].append("unakala2")
    dicoGroups["users"].append("unakala3")    
    
    r = nklG.post_groups(nklT_test, dicoGroups)
    
    if r.isSuccess:
        print(r)
        idGroup = r.dictVals['payload']['id']
        print("group identifier", idGroup)
        
        return idGroup

    else:
        print(r)
        
        return ""
        
def delete_groups_test():
    
    #pour supprimer un group on va commencer par en creer un
    idGroup = post_groups_test()
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    
    r = nklG.delete_groups(nklT_test, idGroup)
    
    print(r)
    
    
def search_groups_test():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")

    q = "Huma-Num-test-Admin"
    r = nklG.search_groups(nklT_test, q, order="asc", page=1, limit=10)
    
    if r.isSuccess:
        print(r)
        
        print("nb responses finded", len(r.dictVals) )
        for vals in r.dictVals:
            print("-------------------")
            print("id : ", vals['id'])
            print("name:",vals['name'])
            print("type :", vals['type'])
            
            print("users")
            for user in vals['users']:
                print(user)
    else:
        print(r)

#post_groups_test()
#delete_groups_test()
#put_groups_test()
#get_groups_test()
search_groups_test()


