## [0.0.1] - 2021-16-07
### Changed
Refonte du code https://gitlab.huma-num.fr/mnauge/nakalapyconnect intialement didactique pour en faire une librairie directement utilisable via système d'import.
Dans cette refonte, est intégré la possibilité de changer facilement de cible (Nakala_production ou Nakala_test) et sa API-Key associée.


